public with sharing class BarCodeController {
    @AuraEnabled(cacheable=true)
public static list<Product2> returnproduct(String barcode){
try {

String strKey = '%'+barcode+'%';
List<Product2> prodList = [SELECT Id, Name, ProductCode, Description, Barcode_number__c FROM Product2  WHERE Barcode_number__c like :strKey limit 1];
System.debug(prodList);
return prodList;

    
} catch (Exception e) {
    throw new AuraHandledException(e.getMessage());
}
}
}